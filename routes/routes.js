var express = require('express');  
var mongoose = require('mongoose');
var chgpass = require("config/chgpass");
var login = require("config/login");
var register = require("config/register");
var addfarmer = require("config/addfarmer");
var respass = require("config/respass");
Users = require('models/users');
Farmers = require('models/farmers');
/*var API_KEY = process.env.NEXMO_API_KEY;
var API_SECRET = process.env.NEXMO_API_SECRET;
var TO_NUMBER = process.env.EXAMPLE_TO_NUMBER;
var FROM_NUMBER = '917895052263';
const Nexmo = require('nexmo');
const nexmo = new Nexmo({
  apiKey: 'ae9f46f2',
  apiSecret: '25e63ea518ece774'
}, {debug: true}); 
*/

module.exports = function(app) {

	app.get('/', function(req, res){
		res.send("Farmss Server Online Now.......");
	});

	//Login Request
	app.post('/login', function(req,res){

		var email = req.body.email;
		var password = req.body.password;

		login.login(email, password, function(found){
			console.log(found);
			res.json(found);
		});
	});

	//Registration Request
	app.post('/register', function(req, res){
	
		var name = req.body.employeeName;
		var email = req.body.email;
		var mobile = req.body.mobile;
		var password = req.body.password;
		var location = req.body.workLocation;

		register.register(name, email, mobile, password, location, function(found){
			console.log(found);	
			res.json(found);
		});
	});

	//Get all the users i.e. the service people
	app.get('/users', function(req, res){
		Users.find(function(err, users){
			if (err)
				res.send(err);
			res.json(users);
		});
	});

	//Find service person by id
	app.get('/users/:user_id', function(req, res){
		Users.findById(req.params.user_id, function(err, user){
			if(err)
				res.send(err);
			res.json(user);
		});
	});

	//updating user information
	app.put('/users/:user_id', function(req, res){
		Users.findById(req.params.user_id, function(err, user){
			user.name = req.body.employeeName;
			user.email = req.body.email;
			user.mobile = req.body.mobile;
			user.location = req.body.workLocation;

			user.save(function(err){
				if (err)
					res.send(err);
				res.json({message: 'User Updated...'});
			});
		});
	});

	//Removing a particular user
	app.delete('/users/:user_id', function(req, res){
		Users.remove({
			_id: req.params.user_id
		}, function(err, user){
			if (err)
				res.send(err);
			res.json({message: 'User Successfully Removed!!'});
		});
	});

	//Request for adding a new farmer
	app.post('/addfarmer', function(req, res){
	
		var name = req.body.farmerName;
		var mobile = req.body.mobile;
		var state = req.body.state;
		var district = req.body.district;
		var tehsil = req.body.tehsil;
		var village = req.body.village;
		var pincode = req.body.pincode;

		addfarmer.addfarmer(name, mobile, state, district, tehsil, village, pincode, function(found){
			console.log(found);
			res.json(found);
		});
	});

	app.post('/addfarmer/bulk', function(req, res){

		if (req.body.farmers) {
			console.log(req.body.farmers);
			Farmers.create(req.body.farmers, function(err){
				if (err)
					res.send(err);
				else
					res.json({'response': true});
			});
		}
		else{
			var newFarmer = new Farmers(req.body);
			newFarmer.save(function(err){
				if (err)
					res.send(err);
				else
					res.json({'response': true});
			});
		}

/*		var farmers = [];

		farmers = req.body.farmers;

		addfarmer.addfarmer_bulk(farmers, function(found){
			console.log(found);
			res.json(found);
		});*/
	});

	//Get all the farmers registered in our system
	app.get('/farmers', function(req, res){
		Farmers.find(function(err, farmers){
			if (err)
				res.send(err);
			res.json(farmers);
		});
	});

	//Return farmer by their id
	app.get('/farmers/:farmer_id', function(req, res){
		Farmers.findById(req.params.farmer_id, function(err, farmer){
			if(err)
				res.send(err);
			res.json(farmer);
		});
	});

	app.put('/farmers/:farmer_id', function(req, res){
		Farmers.findById(req.params.farmer_id, function(err, farmer){
			farmer.name = req.body.farmerName;
			farmer.mobile = req.body.mobile;
			farmer.state = req.body.state;
			farmer.district = req.body.district;
			farmer.tehsil = req.body.tehsil;
			farmer.village = req.body.village;
			farmer.pincode = req.body.pincode;

			farmer.save(function(err){
				if (err)
					res.send(err);
				res.json({message: 'Farmer Updated...'});
			});
		});
	});

	app.delete('/farmers/:farmer_id', function(req, res){
		Farmers.remove({
			_id: req.params.farmer_id
		}, function(err, farmer){
			if (err)
				res.send(err);
			res.json({message: 'Farmer Successfully Removed!!'});
		});
	});


/*	app.post('/send', function(req, res){
		nexmo.message.sendSms(
			process.env.FROM_NUMBER, req.body.toNumber, req.body.message, {type: 'unicode'}, function(err, responseData){
				if (err) 
					console.log(err);
				res.json(responseData);
			});
	}); */

	app.post('/chgpass', function(req, res){
		var id = req.body.id;
		var opass = req.body.oldpass;
		var npass = req.body.newpass;

		chgpass.cpass(id, opass, npass, function(found){
			console.log(found);
			res.json(found);
		});
	});

	app.post('/resetpass', function(req, res){
		var email = req.body.email;

		respass.respass_init(email, function(found){
			console.log(found);
			res.json(found);
		});
	});

	app.post('/resetpass/chg', function(req, res){
		var email = req.body.email;
		var code = req.body.code;
		var npass = req.body.newpass;

		respass.respass_chg(email, code, npass, function(found){
			console.log(found);
			res.json(found);
		});
	});
}